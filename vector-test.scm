(load "list.scm")
(load "vector.scm")
(load "test.scm")

(test #(4 6) (vector-map + #(1 2 3) #(3 4)))

(test 8 (vector-fold + 0 #(3 7 -2)))
(test 6 (vector-fold * 2 #(1 3)))
