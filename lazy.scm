(define-syntax delay-force
  (syntax-rules ()
    ((delay-force expression)
     (%make-promise #f (lambda () expression)))))

(define-syntax delay
  (syntax-rules ()
    ((delay expression)
     (delay-force (%make-promise #t expression)))))

(define (force promise)
  (if (promise? promise)
      (if (%promise-done? promise)
          (%promise-value promise)
          (let ((promise* ((%promise-value promise))))
            (if (not (%promise-done? promise))
                (%promise-update! promise* promise))
            (force promise)))
      promise))

(define (promise? obj)
  (and (pair? obj)
       (eq? (car obj) %promise)))

(define (make-promise obj)
  (if (promise? obj)
      obj
      (%make-promise #t obj)))

(define (%make-promise done? proc)
  (cons %promise (cons done? proc)))

(define (%promise-done? promise)
  (cadr promise))

(define (%promise-value promise)
  (cddr promise))

(define (%promise-update! new old)
  (set-car! (cdr old) (%promise-done? new))
  (set-cdr! (cdr old) (%promise-value new))
  (set-cdr! new (car old)))

(define %promise (list 'promise))
