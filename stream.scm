;;; Syntax

(define-syntax %stream-delay-force
  (syntax-rules ()
    ((%stream-delay-force expr)
     (%make-stream (delay-force (%stream-promise expr))))))

(define-syntax %stream-delay
  (syntax-rules ()
    ((%stream-delay expr)
     (%make-stream
      (delay-force
       (%make-stream (make-promise expr)))))))

;;; Exported syntax

(define-syntax stream-lambda
  (syntax-rules ()
    ((stream-lambda formals body1 body2 ...)
     (lambda formals
       (%stream-delay-force
        (let () body1 body2 ...))))))

(define-syntax stream-cons
  (syntax-rules ()
    ((stream-cons object stream)
     (%make-stream (make-promise (cons (delay object)
                                       (%stream-delay-force stream)))))))

;;; Exported procedures

(define (stream? obj)
  (and (pair? obj)
       (eq? (car obj) %stream)))

(define (stream-pair? obj)
  (and (stream? obj)
       (pair? (%stream-force obj))))

(define (stream-car stream)
  (force (car (%stream-force stream))))

(define (stream-cdr stream)
  (cdr (%stream-force stream)))

(define (stream-null? stream)
  (and (stream? stream)  
       (null? (%stream-force stream))))

;;; Utility procedures

(define (%make-stream promise)
  (cons %stream promise))

(define (%stream-promise stream)
  (cdr stream))

(define (%stream-force stream)
  (force (%stream-promise stream)))

;;; Secret values

(define %stream (list 'stream))

;;; Exported values

(define stream-null (%stream-delay '()))

;;; Derived syntax

(define-syntax define-stream
  (syntax-rules ()
    ((define-stream (name . formals)
       body1 body2 ...)
     (define name
       (stream-lambda formals body1 body2 ...)))))

(define-syntax stream
  (syntax-rules ()
    ((stream)
     stream-null)
    ((stream x y ...)
     (stream-cons x (stream y ...)))))
  
;;; Derived procedures

(define-stream (list->stream objs)
  (if (null? objs)
      stream-null
      (stream-cons (car objs) (list->stream (cdr objs)))))

