#!/bin/bash

git submodule init
git submodule update

export TEXINPUTS=uebungen/contrib/minted/source:uebungen/contrib/fvextra/fvextra:uebungen:
latexmk -shell-escape -pdf uebungen/main.tex
