(define (any pred list)
  (let loop ((list list))
    (and (not (null? list))
         (or (pred (car list))
             (loop (cdr list))))))

(define (every pred list)
  (let loop ((list list) (value #t))
    (if (null? list)
        value
        (let ((value (pred (car list))))
          (and value
               (loop (cdr list)
                     value))))))

(define (fold-right proc acc lst)
  (let loop ((lst lst))
    (if (null? lst)
        acc
        (proc (car lst)
              (loop (cdr lst))))))

(define (fold proc acc lst)
  (let loop ((acc acc) (lst lst))
    (if (null? lst)
        acc
        (loop (proc (car lst) acc)
              (cdr lst)))))





