(define (current-error-handler)
  (lambda (error-object)
    (let ((message (cadr error-object))
          (irritants (cddr error-object)))
      (let ((port (current-error-port)))
        (display message port)
        (cond
          ((null? irritants)
           (newline port))
          ((null? (cdr irritants))
           (display ": " port)
           (display (car irritants) port)
           (newline port))
          (else
           (display ":" port)
           (newline port)
           (for-each (lambda (irritant)
                       (display "  " port)
                       (display irritant port)
                       (newline port))
                     irritants)))))
    error-object))

(define (with-error-handler handler thunk)
  (call-with-current-continuation
   (lambda (exit)
     (handler
      (call-with-current-continuation
       (lambda (fail) 
         (let ((previous-error-handler
                (current-error-handler)))
           (dynamic-wind
            (lambda ()
              (set! current-error-handler
                    (lambda ()
                      (lambda (error-object)
                        (fail error-object)))))
            (lambda ()
              (exit (thunk)))
            (lambda ()
              (set! current-error-handler
                    (lambda ()
                      previous-error-handler)))))))))))

(define (error message . irritants)
  ((current-error-handler)
   `(error ,message . ,irritants)))
