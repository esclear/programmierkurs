(load "lazy.scm")

(load "test.scm")

(test 3 (force (delay (+ 1 2))))

(test '(3 3) (let ((p (delay (+ 1 2))))
               (list (force p) (force p))))

(define integers
  (letrec ((next
            (lambda (n)
              (delay (cons n (next (+ n 1)))))))
    (next 0)))
(define head
  (lambda (stream) (car (force stream))))
(define tail
  (lambda (stream) (cdr (force stream))))
(test 2 (head (tail (tail integers))))

(define (stream-filter p? s)
  (delay-force
   (if (null? (force s))
       (delay '())
       (let ((h (car (force s)))
             (t (cdr (force s))))
         (if (p? h)
             (delay (cons h (stream-filter p? t)))
             (stream-filter p? t))))))
(test 5 (head (tail (tail (stream-filter odd? integers)))))

(define count 0)
(define p
  (delay (begin (set! count (+ count 1))
                (if (> count x)
                    count
                    (force p)))))
(define x 5)
(test #t (promise? p))
(test 6 (force p))
(test #t (promise? p))
(test 6 (begin (set! x 10)
               (force p)))
