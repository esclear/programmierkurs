# Programmierkurs (Scheme, 27. Februar bis 10. März)

Willkommen zum Scheme-Programmierkurs! Wir freuen uns sehr, dass ihr dabei
seid, und hoffen, dass ihr viel Spaß und Freude am Kurs haben und eine Menge
mitnehmen werdet. Wir möchten im Kurs unter anderem folgende Themen behandeln:

* Grundlagen der Scheme-Programmierung: Ausdrücke, Definitionen, Rekursion
* Datenstrukturen
* Veränderliche Variablen
* Ein- und Ausgabe
* Umgang mit der Standardbibliothek: numerische Plots, Interaktion mit
  Webseiten, Netzwerkprogrammierung
* Metalinguistische Abstraktion und Interpreterbau

Je nach euren Interessen können wir Schwerpunkte legen, weitere Inhalte dazu
nehmen oder Inhalte streichen. Es wird eine vielfältige Auswahl an Projekten
geben. Eigene Vorschläge sind sehr willkommen; manche unsere Ideen sind:

* Differentiation (symbolisch und automatisch) und Termvereinfachung
* Matrixkalkül
* Monte-Carlo-Simulationen
* Simulation digitaler Schaltkreise
* Numerische Behandlung gewöhnlicher Differentialgleichungen
* Spiele: Hangman, MasterMind, …
* Warteschlangen und andere Datenstrukturen
* Implementierung des λ-Kalküls
* Knobelaufgaben von [Project Euler](https://projecteuler.net/)

**Der Kurs lebt davon, dass ihr euch während der gesamten Kurszeit
gegenseitig helft und unterstützt.** Insbesondere sind diejenigen von euch
aufgefordert, die schon Programmiervorerfahrung haben; bitte greift denjenigen,
die Programmierneulinge sind, unter die Arme und schaut euch stets aktiv nach
fragenden Gesichtern um.


## Ort und Zeit

* Unser Vorschlag: 9:00 Uhr bis 11:30 Uhr, 12:30 Uhr bis 14:30 Uhr, 15:00 Uhr bis 16:30 Uhr
* Raum 2040/L an allen Tagen, mit Ausnahme vom 2., 7., 9. und 10. März: dann im Raum
  1015/L
* Mindestens fünf von euch müssen mit ihrem eigenen Laptop zum Programmierkurs
  kommen. Allen anderen genügt Stift und Papier.


## Übungsaufgaben

Da Scheme gerne als Lehrsprache eingesetzt wird, gibt es zahlreiche
Übungsaufgaben. [Unsere Sammlung ist hier auf GitLab im
Entstehen.](https://algebra-und-zahlentheorie.gitlab.io/programmierkurs/uebungen.pdf)
Tippfehlerkorrekturen und Ergänzungen sind sehr willkommen!


## Programmierumgebung

Wir verwenden die [Racket-Programmierumgebung](https://racket-lang.org/).
Racket ist freie Software. Sie kann kostenlos heruntergeladen werden und ist
für Linux, Mac und Windows verfügbar.


## Literatur

* Das Wizard-Buch: [Structure and Interpretation of Computer
  Programs](https://mitpress.mit.edu/sicp/full-text/book/book.html)

* [An Introduction to Racket with Pictures](https://docs.racket-lang.org/quick/)

* [Revised⁵ Report on the Algorithmic Language Scheme](http://www.schemers.org/Documents/Standards/R5RS/r5rs.pdf)
