;; https://gitlab.com/algebra-und-zahlentheorie/programmierkurs/raw/master/vector.scm

(define (vector-map proc . vectors)
  (let ((d (fold (lambda (vec d)
                   (min (vector-length vec) d))
                 (vector-length (car vectors))
                 (cdr vectors))))
    (do ((vec (make-vector d))
         (i 0 (+ i 1)))
      ((= i d) vec)
      (vector-set! vec
                   i
                   (apply proc
                          (map (lambda (vec)
                                 (vector-ref vec i))
                               vectors))))))
            
(define (vector-fold proc acc vec)
  (let ((n (vector-length vec)))
    (let loop ((i 0)
               (acc acc))
      (if (= i n)
          acc
          (loop (+ i 1)
                (proc (vector-ref vec i) acc))))))


